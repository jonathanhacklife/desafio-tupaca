const statusService = require('../services/statusService')
const taskService = require('../services/taskService')
const logging = require('../logging')

const getStatuses = async (req, res) => {
  logging.log('debug', 'Obteniendo tareas...')

  try {
    const statuses = await statusService.getStatuses()
    return res.json(statuses)
  } catch (error) {
    // Check if the error has a response status
    if (error.response && error.response.status === 404) {
      return res.status(404).json({ error: 'Task not found' });
    }
  }
}

const filterTasks = async (req, res) => {
  const filters = req.query;
  logging.log('debug', `Calling filterTasks ${filters}`)
  const tasksList = taskService.filterTasks(filters)
  return res.json(statusService.filterTasks(tasksList))
}

const sortTaskBy = async (req, res) => {
  const { key, order } = req.query;
  if (!key || !order) {
    return res.status(400).json({ error: 'Parámetros de ordenamiento incorrectos' });
  }
  logging.log('debug', 'Calling sortTaskBy')
  const tasksList = taskService.sortTaskBy(key, order)
  return res.json(await statusService.updateStatus(tasksList))
}

const createDefaultStatuses = async (req, res) => {
  logging.log('debug', 'Calling createDefaultStatuses')
  const tasksList = await taskService.getAllTasks()
  return res.json(await statusService.createDefaultStatuses(tasksList))
}

const addStatus = async (req, res) => {
  logging.log('debug', 'Calling addTask')
  return res.json(await statusService.addStatus(req.body))
}

const updateStatus = async (req, res) => {
  logging.log('debug', 'Calling updateTask')
  return res.json(await statusService.updateStatus(req.body))
}

module.exports = {
  sortTaskBy,
  filterTasks,
  getStatuses,
  createDefaultStatuses,
  addStatus,
  updateStatus
}
