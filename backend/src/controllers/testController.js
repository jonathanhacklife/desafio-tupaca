﻿const logging = require('../logging')
const { pool } = require('../models/db')

exports.healthCheck = (req, res) => {
  logging.log('debug', 'Health check requested')
  return res.json({ status: 'ok' })
}
exports.receiveText = (req, res) => {
  const respuesta = req.body;
  console.log(respuesta);

  return res.json({ message: 'Text received successfully' });
}

/* exports.testDB = (req, res) => {

  // Get a connection from the pool
  pool.getConnection((err, connection) => {
    // If there's an error getting a connection, return an error message
    if (err) return res.status(500).json({ error: 'Database connection error' });
    console.log('Database connected successfully');

    // Execute the select query
    connection.query('SELECT * FROM tareas', (err, result) => {
      // If there's an error executing the query, return an error message
      if (err) return res.status(500).json({ error: 'Database query error' });

      // Log a success message and send the fetched data as a response
      res.json(result);
      console.log('Query executed successfully');

      // Release the connection
      connection.release();
    });
  })
} */

exports.insertArchivo = (req, res) => {
  // Get a connection from the pool
  pool.getConnection((err, connection) => {
    if (err) return res.status(500).json({ error: 'Database connection error' });
    console.log('Database connected successfully');

    // Obten el objeto que deseas insertar
    const archivo = req.body;

    // Ejecuta la consulta INSERT
    connection.query('INSERT INTO Archivos SET ?', archivo, (err, result) => {
      if (err) return res.status(500).json({ error: 'Database query error' });

      // Log a success message and send the inserted ID as a response
      res.json({ id: result.insertId });
      console.log('Archivo insertado correctamente');

      // Release the connection
      connection.release();
    });
  })
}