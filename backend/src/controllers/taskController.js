const taskService = require('../services/taskService')
const statusService = require('../services/statusService')
const logging = require('../logging')

function parseSortByParameter(sortBy) {
  const [key, order] = sortBy.split(':');
  return { key, order };
}

async function getFilteredAndSortedTasks(req, res) {
  const { sortBy, mode, ...filters } = req.query

  try {
    let tasks = await taskService.getAllTasks()

    if (Object.keys(filters).length > 0) {
      logging.log('debug', 'filters: %o', filters)
      tasks = taskService.filterTasks(filters, mode)
    }

    if (sortBy) {
      const { key, order } = parseSortByParameter(sortBy)
      if (key && order) {
        tasks = await sortTaskBy(tasks, key, order)
      } else {
        return res.status(400).json({ error: 'Parámetros de ordenamiento incorrectos' })
      }
    }

    res.json(tasks)
  } catch (error) {
    return res.status(500).json({ error: 'Error al obtener tareas' })
  }
}

const getAllTasks = async (req, res) => {
  logging.log('debug', 'Getting tasks...')

  try {
    const tasks = await taskService.getAllTasks()
    return res.json(tasks)
  } catch (error) {

    if (error.response) {
      const { status } = error.response;
      if (status === 403) {
        return res.status(403).json({ error: 'Forbidden' });
      } else if (status === 404) {
        return res.status(404).json({ error: 'Tasks not found' });
      }
    }

    return res.status(500).json({ error: 'Server error' })
  }
}

const getTask = async (req, res) => {
  const taskID = req.body?.id;
  logging.log('debug', `Geting task ID: ${taskID}`);
  const task = await taskService.getTask(taskID);
  return res.json(task);
};

const sortTaskBy = async (tasks, key, order) => {
  if (!key || !order) {
    return res.status(400).json({ error: 'Parámetros de ordenamiento incorrectos' });
  }

  let sortedTasks = [...tasks];

  sortedTasks.sort((a, b) => {
    if (typeof a[key] === 'string' && typeof b[key] === 'string') {
      if (order === 'asc') {
        return a[key].localeCompare(b[key]);
      } else if (order === 'desc') {
        return b[key].localeCompare(a[key]);
      }
    } else {
      if (order === 'asc') {
        return a[key] - b[key];
      } else if (order === 'desc') {
        return b[key] - a[key];
      }
    }
  });

  return sortedTasks
}

const addTask = async (req, res) => {
  const { body } = req;
  logging.log('debug', `Adding task ${body}`);
  const tasksList = await taskService.addTask(body);
  const updatedTasksList = await statusService.updateStatus(tasksList);
  return res.json(updatedTasksList);
};

const updateTask = async (req, res) => {
  const { id } = req.params;
  const { body } = req;

  logging.log('debug', `Updating task ID: ${id} Body: ${body}`);
  const updatedTask = await taskService.updateTask(id, body);
  const updatedStatus = await statusService.updateStatus(updatedTask);

  return res.json(updatedStatus);
}

const deleteTask = async (req, res) => {
  const { id } = req.params;
  logging.log('debug', `Deleting task ${id}`)
  const tasksList = await taskService.deleteTask(id)
  return res.json(await statusService.updateStatus(tasksList))
}

module.exports = {
  getFilteredAndSortedTasks,
  getAllTasks,
  getTask,
  sortTaskBy,
  addTask,
  updateTask,
  deleteTask
}
