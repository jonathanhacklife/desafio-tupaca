const express = require('express')
const asyncHandler = require('express-async-handler')
const healthController = require('../controllers/healthController')
const statusController = require('../controllers/statusController')
const taskController = require('../controllers/taskController')
const testController = require('../controllers/testController')

let router = express.Router()

router.route('/health').get(healthController.healthCheck)
router.route('/test').post(asyncHandler(testController.receiveText))
router.route('/testDB').post(asyncHandler(testController.insertArchivo))

/* FIXME put your real routes here */
router.route('/status')
  .get(asyncHandler(statusController.getStatuses))
  .post(asyncHandler(statusController.addStatus))
  .put(asyncHandler(statusController.updateStatus))
  .delete(asyncHandler(statusController.deleteStatus))

router.get('/status/sort', asyncHandler(statusController.sortTaskBy))
router.get('/status/filter', asyncHandler(statusController.filterTasks))

router.route('/tasks')
  .get(asyncHandler(taskController.getFilteredAndSortedTasks))
  .post(asyncHandler(taskController.addTask))

router.route('/tasks/:id')
  .get(asyncHandler(taskController.getTask))
  .put(asyncHandler(taskController.updateTask))
  .delete(asyncHandler(taskController.deleteTask))

router.route('/createBoard')
  .post(asyncHandler(statusController.createDefaultStatuses))
module.exports = router
