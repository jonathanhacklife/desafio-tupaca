const logging = require('../logging')
let nextTaskID = 0


let tasksList = [
  /* Bloquear este bloque para quitar las tareas por defecto */
  /* {
    id: nextTaskID++,
    title: 'Tarea uno',
    description: 'Default Task 1 description',
    created_at: "2024-01-17T03:10:20.157Z",
    priority: 0,
    status: 0
  }, */
]

const validateTaskSchema = (item) => {
  const requiredProperties = ['title', 'description', 'status']
  const missingProperties = requiredProperties.filter(property => !(item.hasOwnProperty(property) || item[property] === 0))

  if (missingProperties.length > 0) {
    throw new Error(`Missing properties: ${missingProperties.join(', ')}`)
  }
}

const isTaskExists = (id) => {
  const foundTask = tasksList.find((task) => task.id == id)
  if (!foundTask) throw new Error('Task ID not found')
  return foundTask
}

const sortTaskBy = (key, order) => {
  let sortedTasks = [...tasksList];

  sortedTasks.sort((a, b) => {
    if (typeof a[key] === 'string' && typeof b[key] === 'string') {
      if (order === 'asc') {
        return a[key].localeCompare(b[key]);
      } else if (order === 'desc') {
        return b[key].localeCompare(a[key]);
      }
    } else {
      if (order === 'asc') {
        return a[key] - b[key];
      } else if (order === 'desc') {
        return b[key] - a[key];
      }
    }
  });

  return sortedTasks;
}

const filterTasks = (querys, mode) => {
  if (Object.keys(querys).length === 0) {
    return tasksList;
  }

  const filteredTasks = tasksList.filter(task => {
    const entries = Object.entries(querys)

    const isMatch = ([key, value]) => {
      return task[key].toString().toLowerCase().includes(value.toLowerCase());
    }

    if (mode === "strict") {
      return entries.every(isMatch);
    } else {
      return entries.some(isMatch);
    }
  });

  return filteredTasks
}

const getAllTasks = async () => tasksList

const getTask = async (id) => isTaskExists(id)

const addTask = async (item) => {
  validateTaskSchema(item)
  const newTask = {
    id: nextTaskID++,
    title: item.title || 'New task',
    description: item.description || '',
    status: item.status || 0,
    priority: item.priority || 0,
    due_date: item.due_date || null,
    created_at: new Date().toJSON(),
  };

  tasksList.push(newTask);
  return tasksList;
};

const updateTask = async (id, itemContent) => {
  const foundTask = isTaskExists(id)
  //validateTaskSchema(itemContent)
  tasksList[foundTask.id] = { ...foundTask, ...itemContent }
  return tasksList
}

const deleteTask = async (id) => {
  const foundTask = isTaskExists(id)
  tasksList = tasksList.filter((task) => task.id != foundTask.id)
  return tasksList
}


module.exports = { sortTaskBy, filterTasks, getAllTasks, getTask, addTask, updateTask, deleteTask }
