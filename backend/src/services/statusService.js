const logging = require('../logging')

let nextTableId = 0

let statusList = []

const filterTasks = (tasksList) => {
  logging.log('debug', 'Updating task %o', tasksList)

  const filteredList = statusList.map((status) => {
    return {
      ...status,
      tasks: [...tasksList].filter((task) => task?.status == status.id)
    }
  })

  return filteredList
}

const getStatuses = async () => statusList

const createDefaultStatuses = async (tasks) => {
  if (statusList.length > 0) throw new Error('Statuses already created')
  statusList = [
    {
      id: nextTableId++,
      title: '🟣 Por hacer',
      description: 'Tareas por hacer',
      tasks: filterTasksByStatus(tasks, 0)
    },
    {
      id: nextTableId++,
      title: '🟠 En progreso',
      description: 'Tareas en progreso',
      tasks: filterTasksByStatus(tasks, 1)
    },
    {
      id: nextTableId++,
      title: '🟢 Hecho',
      description: 'Tareas finalizadas',
      tasks: filterTasksByStatus(tasks, 2)
    }
  ]
}

const addStatus = async (item) => {
  logging.log('debug', 'Adding item %o', item)
  item.id = nextTableId++
  item.title = item?.title ?? 'New status'
  item.description = item?.description ?? ''
  item.tasks = []
  statusList.push(item)
  return item
}

const updateStatus = async (tasksList) => {
  logging.log('debug', 'Updating task %o', tasksList)

  statusList = statusList.map((status) => {
    return {
      ...status,
      tasks: [...tasksList].filter((task) => task?.status == status.id)
    }
  })

  logging.log('debug', 'Updated status %o', statusList)

  return statusList
}

const deleteStatus = async (id) => {
  logging.log('debug', 'Deleting task %o', id)
  statusList = statusList.filter((statusGroup) => statusGroup.id !== id)
  return id
}

module.exports = { filterTasks, getStatuses, createDefaultStatuses, addStatus, updateStatus, deleteStatus }
