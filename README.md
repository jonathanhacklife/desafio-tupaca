# Proyecto con Vite, React, Node y Express

Este es un README para levantar el proyecto. Acá se encuentran los pasos necesarios para configurar el entorno de desarrollo y ejecutar el proyecto.

## Requisitos previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

- Node.js (v20.10.0): [Descargar e instalar Node.js](https://nodejs.org/es/)
- NPM: Normalmente se instala automáticamente con Node.js, pero podés verificar si está instalado ejecutando el siguiente comando en la terminal:

```shell
npm -v
```

Si no está instalado, podés seguir las instrucciones en la [documentación oficial de NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) para instalarlo.

## Configuración del proyecto

1. Cloná este repositorio en tu máquina local o descarga el código fuente.
2. Abrí una terminal y navegá hasta la carpeta raíz del proyecto.
3. Ejecutá el siguiente comando para instalar las dependencias del cliente (React) y las dependencias del servidor (Node y Express):

```shell
npm install
cd backend && npm install
```

Una vez que se completen las instalaciones, deberás configurar las variables de entorno necesarias. Creá un archivo .env en la carpeta raíz del proyecto y definí las variables de entorno requeridas. Podés consultar la documentación de Vite y Express para obtener más información sobre las variables de entorno necesarias.

Ahora estás listo para ejecutar el proyecto. Ejecutá los siguientes comandos para iniciar el proyecto:

**FRONTEND:**
```shell
npm run dev
```
**BACKEND:**
```shell
cd backend && npm run dev
```

Esto iniciará el entorno de backend y frontend. Podrás acceder a la aplicación en http://localhost:8009 y http://localhost:5173/challenge

## Estructura del proyecto
El código fuente de la aplicación Frontend se encuentra en la carpeta src.
El código fuente del backend se encuentra en la carpeta backend.
Podés personalizar la configuración de Vite en el archivo vite.config.js.
Podés personalizar la configuración de Express en el archivo server/index.js.

## Pruebas
En la carpeta "docs" se encuentra un archivo para importar en Postman y poder probar los endpoints generados en el proyecto.