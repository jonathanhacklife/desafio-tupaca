﻿import axios from 'axios'
import { useEffect } from 'react'
import { useMutation, useQuery } from '@tanstack/react-query'

const apiUrl = import.meta.env.VITE_API_URL

/* TASKS LIST */
export const getTasksAPI = async (filter = {}, sort = "") => {
  try {
    const params = new URLSearchParams(Object.entries(filter).reduce((obj, [key, value]) => {
      if (value !== "" && value !== null) {
        obj[key] = value;
      }
      return obj;
    }, {}));
    const queryParams = params.toString();
    const sortParams = !!sort ? `sortBy=${sort.key}:${sort.order}` : "";
    const fullParams = [queryParams, sortParams].filter(Boolean).join("&");

    const url = `${apiUrl}/tasks?${fullParams}`;

    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    console.error('Error:', error);
    throw error;
  }
}

/* SORT TASKS */
export const sortTaskAPI = async (key, order) => {
  console.info(`Ordenando tareas...`)
  try {
    const url = `${apiUrl}/status/sort?key=${key}&order=${order}`
    const response = await axios.get(url)
    return response.data
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

/* ADD TASK */
export const addTaskAPI = async (item) => {
  console.table(`Agregando tarea...`, item)
  try {
    const url = `${apiUrl}/tasks`
    const response = await axios.post(url, item)
    return response.data
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

/* UPDATE TASK */
export const updateTaskAPI = async (taskID, taskContent) => {
  console.info(`Actualizando tarea...${taskID}`)
  try {
    const url = `${apiUrl}/tasks/${taskID}`
    const response = await axios.put(url, taskContent)
    return response.data
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

/* DELETE TASK */
export const deleteTaskAPI = async (id) => {
  console.info(`Eliminando tarea...${id}`)
  try {
    const url = `${apiUrl}/tasks/${id}`
    const response = await axios.delete(url)
    return response.data
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

/* API HOOK */
export const useTasksList = (filter = {}, sort = "") => {
  const dataQuery = useQuery({
    queryKey: ['getTasks', filter],
    queryFn: () => getTasksAPI(filter, sort),
    options: {
      refetchOnWindowFocus: false,
      initialData: [{}]
    }
  })

  useEffect(() => {
    dataQuery.refetch()
  }, [filter, sort])

  return dataQuery
}

const useTasksMutations = () => {
  const getTask = useMutation({ mutationFn: (id) => getTasksAPI(id) })
  const sortTasks = useMutation({ mutationFn: (values) => sortTaskAPI(values.key, values.order) })
  const addTask = useMutation({ mutationFn: (tarea) => addTaskAPI(tarea) })
  const updateTask = useMutation({ mutationFn: (values) => updateTaskAPI(values.id, values.task) })
  const deleteTask = useMutation({ mutationFn: (id) => deleteTaskAPI(id) })
  return { getTask, sortTasks, addTask, updateTask, deleteTask }
}

export default useTasksMutations