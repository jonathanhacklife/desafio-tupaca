﻿import React, { useState } from 'react'

// UI COMPONENTS
import MenuIcon from '@mui/icons-material/Menu'
import { AppBar, Box, Button, IconButton, Link, Stack, Toolbar, Typography } from '@mui/material'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import logoLabs from '../../../public/Logo-1.svg'
import Logo from './Logo'


// ICONS

// APIS

// PROVIDERS

// UTILS


const Navbar = () => {

  const pages = [
    { path: '/', label: 'Home', show: true },
    { path: '/challenge', label: 'Challenge', show: true },
  ]

  const [anchorElNav, setAnchorElNav] = useState(null)

  const handleOpenNavMenu = () => {
    setAnchorElNav(event.currentTarget)
  }

  const handleCloseNavMenu = () => {
    setAnchorElNav(null)
  }

  return (
    <AppBar position="fixed" id="navbar" >
      <Toolbar variant="dense">
        {/* MOBILE */}

        {/* LOGO */}
        <Stack direction="row" justifyContent="space-between" spacing={1} sx={{ flexGrow: 1, display: { md: 'none' } }}>
          <Logo imgUrl={logoLabs} alt="logo" sx={{ mr: 2 }} />
          <Box>
            <IconButton onClick={handleOpenNavMenu}
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit">
              <MenuIcon />
            </IconButton>
            <Menu id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
              keepMounted
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{ display: { xs: 'block', md: 'none' } }}>
              {pages?.filter(page => page?.show).map((page) => (
                <Link key={page.label} href={page.path} sx={{ textDecoration: 'none' }}>
                  <MenuItem sx={{ justifyContent: 'right' }}>
                    <Typography>{page.label}</Typography>
                  </MenuItem>
                </Link>
              ))}
            </Menu>
          </Box>
        </Stack>

        {/* DESKTOP */}

        {/* LOGO */}
        <Logo imgUrl={logoLabs} alt="logo" sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }} />

        {/* MENU */}
        <Stack direction="row" justifyContent="start" spacing={1} sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
          {pages?.filter(page => page?.show).map((page, index) => (
            <Button key={index} className="scale" href={page.path}>
              <Typography variant="body1" color="primary.contrastText">{page.label}</Typography>
            </Button>
          ))}
        </Stack>

      </Toolbar>
    </AppBar >
  )
}

export default Navbar
