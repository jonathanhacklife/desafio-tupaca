﻿import React from 'react'
import { Button, Typography } from '@mui/material'

export default function Label({ title, icon, onClick, backgroundColor = "lightblue", color = "black", variant = "outlined", sx }) {
  const labelStyle = {
    borderRadius: "4px",
    backgroundColor: backgroundColor,
    color: color,
    py: 0.1,
    px: 0.5,
    ...sx,
  }

  return title && <Button sx={labelStyle} disableElevation variant={variant} onClick={onClick}>{icon}<Typography variant="overline" sx={{ lineHeight: 1 }}>{title}</Typography></Button>
}
