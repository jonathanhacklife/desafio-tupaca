﻿import React, { useState } from 'react'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import { MoreVert } from '@mui/icons-material'
import { Box, Divider, IconButton, Stack } from '@mui/material'
import Visible from './Visible'

export default function CustomMenu({ options, size, children }) {
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Visible condition={options?.length}>
        {children
          ? <Box onClick={handleClick}>{children}</Box>
          : <IconButton
            aria-label="more"
            id="long-button"
            aria-controls={open ? 'long-menu' : undefined}
            aria-expanded={open ? 'true' : undefined}
            size={size}
            aria-haspopup="true"
            onClick={handleClick}>
            <MoreVert />
          </IconButton>
        }
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}>
          {options?.map((option, index) => (
            <MenuItem key={option?.label} onClick={() => {
              option?.onSelect()
              handleClose()
            }}>
              {option?.icon && (
                <ListItemIcon>{option?.icon}</ListItemIcon>
              )}
              <Stack direction="row" justifyContent="space-between" width="100%">
                {option?.label}
                {option?.altIcon && (
                  <ListItemIcon sx={{justifyContent: 'end'}}>{option?.altIcon}</ListItemIcon>
                )}
              </Stack>
            </MenuItem>
          ))}
        </Menu>
      </Visible>
    </>
  )
}