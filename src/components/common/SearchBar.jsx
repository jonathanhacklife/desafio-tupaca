﻿import React, { useState } from 'react'
import { Box, InputBase, Button, IconButton, Typography, TextField, Stack, InputAdornment, OutlinedInput, InputLabel, FormControl } from '@mui/material'
import { Clear, FilterAltOff, Search } from '@mui/icons-material'
import Visible from './Visible'

/**
 * Renderiza un componente de barra de búsqueda.
 *
 * @param {string} label - La etiqueta para la barra de búsqueda.
 * @param {function} onSearch - La función de devolución de llamada que se llamará cuando se haga clic en el botón de búsqueda.
 * @param {Object} sx - Estilos adicionales para la barra de búsqueda.
 * @return {JSX.Element} - El componente de barra de búsqueda.
*/
const SearchBar = ({ label = "Search...", onSearch, sx }) => {

  const [searchQuery, setSearchQuery] = useState('')

  const handleSearch = (e) => {
    e?.preventDefault()
    onSearch({ title: searchQuery, description: searchQuery })
  }

  const handleClear = () => {
    setSearchQuery('')
    onSearch({ title: '', description: '' })
  }

  return (
    <form noValidate autoComplete="off" style={{ ...sx, width: '100%' }} onSubmit={handleSearch}>
      <Stack direction="row" alignItems="center" spacing={1}>
        <FormControl fullWidth>
          <InputLabel htmlFor="search-bar">{label}</InputLabel>
          <OutlinedInput
            label={label}
            id="search-bar"
            size="small"
            fullWidth
            value={searchQuery}
            onChange={(event) => setSearchQuery(event.target.value)}
            startAdornment={searchQuery && <InputAdornment position="start">
              <IconButton type="button" aria-label="search" onClick={handleSearch}>
                <Search sx={{ fontSize: 20 }} />
              </IconButton>
            </InputAdornment>}
            endAdornment={
              <InputAdornment position="end">
                <Visible condition={searchQuery}>
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setSearchQuery('')}
                    edge="end"
                  >
                    <Clear />
                  </IconButton>
                </Visible>
              </InputAdornment>
            }
          />
        </FormControl>
      </Stack>
    </form >
  )
}

export default SearchBar