﻿import React from 'react'

// UI COMPONENTS
import { Button, Divider, FormControl, Grid, InputLabel, MenuItem, Select, Stack, TextField, Typography } from '@mui/material'
import Visible from '../common/Visible'
import Modal from './Modal'

// ICONS
import { Flag } from '@mui/icons-material'

// APIS

// PROVIDERS
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers"
import { useResources } from '../../providers/ResourcesProvider'

// UTILS
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import dayjs from "dayjs"
import { useFormik } from "formik"
import * as yup from 'yup'

export default function ModalCreate({ isShowing, hide, readOnly, header, onSubmit }) {
  const MAX_CHARS = 250
  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      priority: "",
      due_date: null,
    },
    validationSchema: yup.object({
      title: yup.string().required("El título es requerido"),
      description: yup.string(),
      priority: yup.string(),
      due_date: yup.mixed().nullable(),
    }),
    onSubmit: (values) => {
      onSubmit(values)
      formik.resetForm()
      hide()
    }
  })

  const remainingChars = MAX_CHARS - formik.values.description.length
  const { resources } = useResources()

  return (
    <Modal isShowing={isShowing}
      hide={hide}
      header={header}
      actions={
        <Visible condition={!readOnly}>
          <Button variant="contained"
            disabled={!formik.values.title}
            onClick={formik.handleSubmit}
            fullWidth>
            Crear
          </Button>
        </Visible>
      }>
      <Stack component="form" onSubmit={formik.handleSubmit} spacing={2}>
        <Grid container gap={2}>

          {/* TÍTULO */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField label="Título"
              variant="outlined"
              name="title"
              disabled={readOnly}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.title}
              fullWidth
              error={Boolean(formik.touched.title && formik.errors.title)}
              helperText={formik.touched.title && formik.errors.title}
              inputProps={{ maxLength: MAX_CHARS }}
            />
          </Grid>

          {/* DESCRIPCIÓN */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField label="Descripción"
              variant="outlined"
              name="description"
              disabled={readOnly}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.description}
              fullWidth
              multiline
              rows={3}
              error={Boolean(formik.touched.description && formik.errors.description)}
              helperText={formik.touched.description && formik.errors.data}
              inputProps={{ maxLength: MAX_CHARS }}
            />
            <Typography variant="caption">{remainingChars} caracteres disponibles</Typography>
          </Grid>

          {/* FECHA DE ENTEGA */}
          <Grid item xs={12} sm md lg>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker placeholder="Fecha de entrega"
                format='DD/MM/YYYY'
                value={formik.values?.due_date && dayjs(formik.values?.due_date)}
                onChange={(value) => formik.setFieldValue('due_date', value)}
                onBlur={formik.handleBlur}
                error={Boolean(formik.touched.due_date && formik.errors.due_date)}
                helperText={formik.touched.due_date && formik.errors.due_date}
                sx={{ width: '100%' }}
              />
            </LocalizationProvider>
          </Grid>

          {/* PRIORIDAD */}
          <Grid item xs={12} sm md lg>
            <FormControl fullWidth>
              <InputLabel id="priority">Prioridad</InputLabel>
              <Select
                label="Prioridad"
                labelId="priority"
                onChange={formik.handleChange("priority")}
                error={Boolean(formik.touched.priority && formik.errors.priority)}
              >
                <MenuItem value="">Ninguna</MenuItem>
                <Divider />
                {resources?.priorities?.map((priority) => (
                  <MenuItem key={priority.id} value={priority.id}>
                    <Flag sx={{ color: `${priority?.color}.main`, mr: 1 }} />{priority.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </Stack>
    </Modal>
  )
}
