﻿import React, { useState } from 'react'

// UI COMPONENTS
import { Button, Divider, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Select, Stack, Switch, TextField, Typography } from '@mui/material'
import Visible from '../common/Visible'
import Modal from './Modal'

// ICONS
import { Flag } from '@mui/icons-material'

// APIS

// PROVIDERS
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers"
import { useResources } from '../../providers/ResourcesProvider'

// UTILS
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import dayjs from "dayjs"
import { useFormik } from "formik"
import * as yup from 'yup'

export default function ModalFilter({ isShowing, hide, readOnly, header, onSubmit }) {
  const MAX_CHARS = 250
  const [mode, setMode] = useState(false)

  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      priority: "",
      created_at: null,
      due_date: null,
      status: ""
    },
    validationSchema: yup.object({
      title: yup.string(),
      description: yup.string(),
      priority: yup.string(),
      created_at: yup.mixed().nullable(),
      due_date: yup.mixed().nullable(),
      status: yup.string(),
    }),
    onSubmit: (values) => {
      const filters = { ...values, mode: mode ? "nested" : "strict" }
      const fieldsWithValue = Object.entries(filters)
        .filter(([key, value]) => value !== "" && value !== null)
        .reduce((obj, [key, value]) => {
          obj[key] = value;
          return obj;
        }, {});

      onSubmit(fieldsWithValue)
      formik.resetForm()
      hide()
    }
  })

  const remainingChars = MAX_CHARS - formik.values.description.length
  const { resources } = useResources()


  return (
    <Modal isShowing={isShowing}
      hide={hide}
      header={header}
      actions={
        <Visible condition={!readOnly}>
          <Button variant="contained" onClick={formik.handleSubmit} fullWidth>Buscar</Button>
        </Visible>
      }>
      <Stack component="form" onSubmit={formik.handleSubmit} spacing={2}>
        <Grid container gap={2}>

          {/* TÍTULO */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField label="Título"
              variant="outlined"
              name="title"
              disabled={readOnly}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.title}
              fullWidth
              error={Boolean(formik.touched.title && formik.errors.title)}
              helperText={formik.touched.title && formik.errors.title}
              inputProps={{ maxLength: MAX_CHARS }}
            />
          </Grid>

          {/* DESCRIPCIÓN */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField label="Descripción"
              variant="outlined"
              name="description"
              disabled={readOnly}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.description}
              fullWidth
              multiline
              rows={3}
              error={Boolean(formik.touched.description && formik.errors.description)}
              helperText={formik.touched.description && formik.errors.data}
              inputProps={{ maxLength: MAX_CHARS }}
            />
            <Typography variant="caption">{remainingChars} caracteres disponibles</Typography>
          </Grid>

          {/* FECHA DE CREACIÓN */}
          <Grid item xs={6} sm={6} md={6} lg={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                format='DD/MM/YYYY'
                value={formik.values?.created_at && dayjs(formik.values?.created_at)}
                onChange={(value) => {
                  const formattedDate = value && value.format('YYYY-MM-DD');
                  formik.setFieldValue('created_at', formattedDate);
                }}
                onBlur={formik.handleBlur}
                error={Boolean(formik.touched.created_at && formik.errors.created_at)}
                helperText={formik.touched.created_at && formik.errors.created_at}
                slotProps={{ textField: { placeholder: "Fecha de creación" } }}
                sx={{ width: '100%' }}
              />
            </LocalizationProvider>
          </Grid>

          {/* FECHA DE ENTREGA */}
          <Grid item xs sm md lg>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                format='DD/MM/YYYY'
                value={formik.values?.due_date && dayjs(formik.values?.due_date)}
                onChange={(value) => {
                  const formattedDate = value && value.format('YYYY-MM-DD');
                  formik.setFieldValue('due_date', formattedDate);
                }}
                onBlur={formik.handleBlur}
                error={Boolean(formik.touched.due_date && formik.errors.due_date)}
                helperText={formik.touched.due_date && formik.errors.due_date}
                slotProps={{ textField: { placeholder: "Fecha de entrega" } }}
                sx={{ width: '100%' }}
              />
            </LocalizationProvider>
          </Grid>

          {/* ESTADO */}
          <Grid item xs={6} sm={6} md={6} lg={6}>
            <FormControl fullWidth>
              <InputLabel id="status">Estado</InputLabel>
              <Select
                label="Estado"
                labelId="status"
                value={formik.values.status}
                onChange={formik.handleChange("status")}
                error={Boolean(formik.touched.status && formik.errors.status)}
              >
                {resources?.statuses?.map((status) => (
                  <MenuItem key={status.id} value={status.id}>{status.title}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>

          {/* PRIORIDAD */}
          <Grid item xs sm md lg>
            <FormControl fullWidth>
              <InputLabel id="priority">Prioridad</InputLabel>
              <Select
                label="Prioridad"
                labelId="priority"
                value={formik.values.priority}
                onChange={formik.handleChange("priority")}
                error={Boolean(formik.touched.priority && formik.errors.priority)}
              >
                {resources?.priorities?.map((priority) => (
                  <MenuItem key={priority.id} value={priority.id}>
                    <Flag sx={{ color: `${priority?.color}.main`, mr: 1 }} />{priority.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>

          {/* MODO DE BÚSQUEDA */}
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <FormControlLabel
              control={<Switch checked={mode} onChange={() => setMode(!mode)} color="primary" />}
              label={mode ? 'Modo Anidado' : 'Modo Estricto'}
            />
          </Grid>
        </Grid>
      </Stack>
    </Modal>
  )
}
