﻿export default function getProgress(array = [], condition) {
  const total = array.length;
  const completed = array.filter(condition).length;
  const percentage = total > 0 ? (completed / total) * 100 : 0;
  return { percentage, completed, total };
}