﻿/**
 * Codifica una URL eliminando caracteres especiales, convirtiéndola a minúsculas y reemplazando espacios con un delimitador dado.
 *
 * @param {string} cadena - La URL que se va a codificar.
 * @param {string} limitador - El delimitador para reemplazar los espacios.
 * @return {string} - La URL codificada.
*/
export default function encodeURL(cadena, limitador = "-") {
  var cadenaSinCaracteresEspeciales = cadena?.replace(/[^\w\s-]/g, "").toLowerCase()
  return encodeURIComponent(cadenaSinCaracteresEspeciales)?.replace(/%20/g, limitador)
}