/**
 * Genera un número entero aleatorio entre el valor mínimo y máximo proporcionado.
 *
 * @param {number} min - El valor mínimo (inclusive).
 * @param {number} max - El valor máximo (exclusivo).
 * @return {number} El número entero aleatorio generado.
*/
export default function getRandomInt(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min) + min) // El máximo es exclusivo y el mínimo es inclusivo
}