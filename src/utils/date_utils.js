﻿/**
 * Analiza una fecha dada y la convierte en una representación de cadena localizada.
 *
 * @param {string} stringDate - La fecha que se va a analizar.
 * @return {string} La representación de cadena localizada de la fecha.
 */
export function parseLocaleDate(stringDate) {
  return new Date(stringDate).toLocaleDateString('es-ES')
}

/**
 * Analiza la fecha proporcionada y la devuelve formateada como 'yyyy-mm-dd'.
 *
 * @param {string} stringDate - La fecha a analizar.
 * @return {string} La fecha formateada en el formato 'yyyy-mm-dd'.
 */
export const formatDate = (date) => {
  const options = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
    timeZone: 'America/Argentina/Buenos_Aires'
  }
  const formattedDate = new Intl.DateTimeFormat('es-AR', options).format(new Date(date))
  return formattedDate
}

export function convertirFecha(fechaStr) {
  const [fecha, hora] = fechaStr.split(' ')
  const [anio, mes, dia] = fecha.split('-').map(Number)
  const [horas, minutos, segundos] = hora.split(':').map(Number)
  return new Date(anio, mes - 1, dia, horas, minutos, segundos)
}
export function mostrarFecha(fecha) {
  const opciones = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' }
  const fechaFormateada = fecha.toLocaleDateString('es-ES', opciones)
  return fechaFormateada
}