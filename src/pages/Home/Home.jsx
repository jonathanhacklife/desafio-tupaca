import React from 'react'

// UI COMPONENTS
import { Box, Link, Stack, Typography } from '@mui/material'
import Navbar from '../../components/common/Navbar'

// ICONS
import labsLogo from '../../../public/Logo-2.svg'
import viteLogo from '../../../public/vite.svg'
import reactLogo from '../../assets/react.svg'

// APIS

// PROVIDERS

// UTILS
import mailTo, { config } from '../../utils/mail'


function Home() {
  return (
    <>
      <Navbar />
      <Stack className="welcome" bgcolor="black" color="secondary.main" justifyContent="center" alignItems="center" spacing={3}>
        <Box>
          <Link href="https://vitejs.dev" target="_blank">
            <img src={viteLogo} className="logo scale" alt="Vite logo" />
          </Link>
          <Link href="https://react.dev" target="_blank">
            <img src={reactLogo} className="logo scale" alt="React logo" />
          </Link>
          <Link href="https://infinitelabs.com.ar" target="_blank">
            <img src={labsLogo} className="logo scale" alt="Infinite Labs logo" />
          </Link>
        </Box>

        <Stack direction="row">
          <Typography variant="h2custom" className="scale">Vite</Typography>
          <Typography variant="h2custom" className="scale">+</Typography>
          <Typography variant="h2custom" className="scale">React</Typography>
          <Typography variant="h2custom" className="scale">+</Typography>
          <Typography variant="h2custom" className="scale"><b>LABS</b></Typography>
        </Stack>


        <Typography variant="body1custom" className="scale">Desafío elaborado para la empresa Tupaca</Typography>

        <Typography variant="body2" className="scale">More info: <Link href={mailTo} target="_blank">{config?.to}</Link></Typography>
      </Stack>
    </>
  )
}

export default Home
