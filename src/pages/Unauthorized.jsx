﻿import React from 'react'
import { Button, Stack, Typography } from '@mui/material'

function Unauthorized() {
  return (
    <Stack justifyContent="center" alignItems="center" spacing={3}>
      <Typography variant="h1">404</Typography>
      <Typography variant="h6">La página que estás buscando no se encuentra.</Typography>
      <Button variant="contained" onClick={() => window.location.href = '/'}>Volver al inicio</Button>
    </Stack>
  )
}

export default Unauthorized