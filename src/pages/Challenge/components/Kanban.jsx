﻿import React, { useState, useEffect } from 'react'

// UI COMPONENTS
import { Alert, Button, Chip, Container, Grid, IconButton, Modal, Stack } from '@mui/material'
import Loader from '../../../components/common/Loader'
import Visible from '../../../components/common/Visible'
import ProgressBar from '../../../components/common/ProgressBar'

// ICONS

// APIS
import useTasksMutations, { useTasksList } from '../../../api/board'

// PROVIDERS
import { useResources } from '../../../providers/ResourcesProvider'

// UTILS
import SearchBar from '../../../components/common/SearchBar'
import StatusColumn from './StatusColumn'
import { ArrowDropDown, ArrowDropUp, FilterAlt, FilterAltOff, SortByAlpha } from '@mui/icons-material'
import useModal from '../../../hooks/useModal'
import ModalFilter from '../../../components/modals/ModalFilter'
import CustomMenu from '../../../components/common/CustomMenu'
import getProgress from '../../../utils/getProgress'
import ModalCreate from '../../../components/modals/ModalCreate'


const KanbanBoard = () => {

  const [filter, setFilter] = useState({})
  const [sorting, setSorting] = useState({ key: "priority", order: "desc" })

  /* API CONTROLLER */
  const mutations = useTasksMutations()
  const { data, refetch, isLoading, isError } = useTasksList(filter, sorting)
  const { resources, setResources } = useResources()

  /* MODALES */
  const modals = {
    createTask: useModal(),
    filterTask: useModal()
  }

  /* OPCIONES */
  const options = {
    properties: resources?.properties?.map(({ label, key }) => ({
      label,
      altIcon: sorting.key === key && (sorting.order === 'asc' ? <ArrowDropUp /> : <ArrowDropDown />),
      onSelect: () => handleSort(key)
    }))
  }

  useEffect(() => {
    const completedIDTask = 2
    setResources({
      completedIDTask,
      selectedFilters: Object.entries(filter).filter(([key, value]) => value !== "" && value !== null && key !== "mode"),
      progress: getProgress(data, (item) => item?.status == completedIDTask).percentage,
      statuses: [
        { id: 0, title: "🟣 Por hacer", description: "" },
        { id: 1, title: "🟠 En progreso", description: "" },
        { id: 2, title: "🟢 Hecho", description: "" }
      ],
      priorities: [
        { id: 0, label: 'No Priority', color: 'secondary' },
        { id: 1, label: 'Low Priority', color: 'info' },
        { id: 2, label: 'Med Priority', color: 'warning' },
        { id: 3, label: 'High Priority', color: 'danger' }
      ],
      properties: [
        { label: "Título", key: "title" },
        { label: "Descripción", key: "description" },
        { label: "Estado", key: "status" },
        { label: "Prioridad", key: "priority" },
        { label: "Fecha de entrega", key: "due_date" },
        { label: "Fecha de creación", key: "created_at" }
      ]
    })
  }, [data])

  /* HANDLERS */
  const handleFilter = (filterData = {}) => {
    setFilter(filterData)
  }

  const handleSort = async (key) => {
    setSorting({ key: key, order: sorting.order === 'asc' ? 'desc' : 'asc' })
  }

  const addTask = async (task = "") => {
    await mutations.addTask.mutateAsync(task)
    refetch()
  }

  if (isLoading) {
    return <Stack height="80vh" justifyContent="center" alignItems="center"><Loader /></Stack>
  }
  if (isError) {
    return <Stack height="80vh" justifyContent="center" alignItems="center">
      <Alert severity="error">
        Ocurrió un error al cargar los datos
      </Alert>
    </Stack>
  }

  return (
    <Container maxWidth="xl">
      <Stack gap={3}>

        {/* BARRA DE BÚSQUEDA */}
        <Stack direction="row" justifyContent="space-between" spacing={1}>
          <SearchBar label="Buscar tareas..." onSearch={handleFilter} />

          {/* FILTRAR POR PROPIEDADES */}
          <Visible condition={data?.length > 0}
            alternative={<IconButton type="button" aria-label="search" onClick={() => handleFilter()}><FilterAltOff /></IconButton>}>
            <IconButton type="button" aria-label="search" onClick={modals.filterTask.open}><FilterAlt /></IconButton>
          </Visible>

          {/* ORDENAR POR PROPIEDADES */}
          <CustomMenu options={options.properties} size="small">
            <IconButton><SortByAlpha /></IconButton>
          </CustomMenu>
        </Stack>

        {/* PROPIEDADES SELECCIONADAS */}
        <Stack direction="row" justifyContent="center" alignItems="center" spacing={1}>
          {resources?.selectedFilters.map(([key, value]) => (
            <Chip key={key}
              label={`${resources?.properties.find(property => property.key === key)?.label}: ${value}`}
              variant="outlined"
              onDelete={() => handleFilter({ ...filter, [key]: "" })}
            />
          ))}
        </Stack>

        {/* PROGRESO DE TAREAS COMPLETADAS */}
        <ProgressBar value={resources?.progress} />

        {/* TABLERO DE ESTADOS */}
        <Grid container justifyContent="center" spacing={3}>
          {resources.statuses.map((status, i) => (
            <StatusColumn key={i}
              status={status}
              onFilter={handleFilter}
              tasks={data?.filter(task => task?.status == status.id)} />
          ))}
        </Grid>
      </Stack>

      {/* MODAL DE FILTROS */}
      <ModalFilter isShowing={modals.filterTask.isOpen}
        hide={modals.filterTask.close}
        header="Filtrar tareas"
        onSubmit={(data) => handleFilter(data)} />

      <ModalCreate isShowing={modals.createTask.isOpen}
        hide={modals.createTask.close}
        header="Nueva tarea"
        onSubmit={(data) => addTask({ ...data, status: 0 })} />

    </Container>
  )
}

export default KanbanBoard