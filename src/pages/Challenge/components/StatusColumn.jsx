﻿import React from 'react';

// UI COMPONENTS
import { Button, Chip, Grid, IconButton, Paper, Stack, Typography } from '@mui/material';
import ModalCreate from '../../../components/modals/ModalCreate';
import TasksCard from './TaskCard';

// ICONS


// APIS
import useTasksMutations from '../../../api/board';

// PROVIDERS

// UTILS
import { useQueryClient } from '@tanstack/react-query';
import useModal from '../../../hooks/useModal';


function StatusColumn({ status, tasks, onFilter }) {

  const modals = {
    newTask: useModal(),
    searchTask: useModal(),
  }
  const mutations = useTasksMutations()
  const queryClient = useQueryClient()

  const addTask = async (task = "") => {
    await mutations.addTask.mutateAsync(task)
    queryClient.refetchQueries('getTasks')
  }

  const searchTask = async (task) => {
    await mutations.searchTask.mutateAsync(params)
    queryClient.refetchQueries('getTasks')
  }

  return (
    <>
      <Grid item xs={12} sm={4} md={4} lg={4}>
        <Paper sx={{ p: 2 }}>
          <Stack spacing={1}>

            {/* HEADER */}
            <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ borderBottom: 1, pb: 1, borderColor: 'divider' }}>
              <Typography variant="h6" gutterBottom>{status.title}</Typography>
              <Chip label={<label>{tasks?.length}</label>} />
            </Stack>

            {/* TAREA */}
            <Stack spacing={1}>
              {tasks?.map((task, i) => <TasksCard key={i} task={task} />)}
            </Stack>

            {/* AGREGAR TAREA */}
            <Button variant="outlined" fullWidth onClick={() => modals.newTask.open(status)}>Agregar tarea</Button>
          </Stack>
        </Paper>
      </Grid>

      <ModalCreate isShowing={modals.newTask.isOpen}
        hide={modals.newTask.close}
        header="Nueva tarea"
        action="Crear"
        onSubmit={(data) => addTask({ ...data, status: modals.newTask.args.id })} />

      <ModalCreate isShowing={modals.searchTask.isOpen}
        hide={modals.searchTask.close}
        header="Buscar tarea"
        action="Buscar"
        onSubmit={(data) => searchTask(data)} />
    </>
  );
}

export default StatusColumn;