﻿import React from 'react'

// UI COMPONENTS
import { IconButton, Stack, Tooltip, Typography } from "@mui/material"
import CustomMenu from "../../../components/common/CustomMenu"
import Label from "../../../components/common/Label"
import ModalEdit from "../../../components/modals/ModalEdit"

// ICONS
import { Delete, Edit, Flag, RadioButtonChecked, Schedule } from "@mui/icons-material"

// APIS
import useTasksMutations from "../../../api/board"

// PROVIDERS
import useModal from "../../../hooks/useModal"
import { useResources } from "../../../providers/ResourcesProvider"

// UTILS
import { useQueryClient } from "@tanstack/react-query"
import { formatDate } from "../../../utils/date_utils"
import Visible from '../../../components/common/Visible'


const TasksCard = ({ task }) => {

  const mutations = useTasksMutations()
  const queryClient = useQueryClient()
  const { resources } = useResources()
  const modals = { editTask: useModal() }


  const updateTask = async (taskID, taskData) => {
    await mutations.updateTask.mutateAsync({ id: taskID, task: taskData })
    queryClient.refetchQueries('getTasks')
  }

  const deleteTask = async (taskID) => {
    await mutations.deleteTask.mutateAsync(taskID)
    queryClient.refetchQueries('getTasks')
  }

  const options = {
    others: [{
      label: 'Editar',
      icon: <Edit />,
      onSelect: () => modals.editTask.open(task)
    },
    {
      label: 'Eliminar',
      icon: <Delete />,
      onSelect: () => deleteTask(task?.id)
    }],
    status: resources?.statuses?.map(status => ({
      label: status.title,
      onSelect: () => updateTask(task?.id, { status: status?.id })
    })) || [],
    priorities: resources?.priorities?.map((priority, index) => ({
      label: priority.label,
      icon: <Flag sx={{ color: `${priority?.color}.main` }} />,
      onSelect: () => updateTask(task?.id, { priority: priority?.id })
    }))
  }

  return (
    <>
      <Stack direction="row" justifyContent="stretch" alignItems="flex-start"
        sx={{ p: 1, borderRadius: 2, border: '1px solid #ccc' }}>
        <Stack p={1} justifyContent="center" alignItems="flex-start" width="100%">

          {/* TITULO */}
          <Stack direction="row" justifyContent="space-between" alignItems="center" width="100%">
            <Typography variant="body1" align="left" sx={{ overflow: 'hidden', textOverflow: 'ellipsis' }}>{task?.title || "Sin título"}</Typography>
            <CustomMenu options={options.others} size="small" />
          </Stack>

          {/* DESCRIPCIÓN */}
          <Typography variant="subtitle2" color="text.secondary" align="left" sx={{ overflow: 'hidden', textOverflow: 'ellipsis', width: "100%" }}>{task?.description}</Typography>

          {/* FECHA DE CREACIÓN */}
          <Visible condition={task?.created_at}>
            <Stack direction="row" spacing={0.5} alignItems="center">
              <Schedule sx={{ fontSize: 15 }} color="text.secondary" />
              <Typography variant="subtitle2" color="text.secondary" align="left">{formatDate(task?.created_at || new Date())}</Typography>
            </Stack>
          </Visible>

          {/* FECHA DE ENTREGA */}
          <Visible condition={task?.due_date}>
            <Stack direction="row" spacing={0.5} alignItems="center">
              <Schedule sx={{ fontSize: 15 }} color="text.secondary" />
              <Typography variant="subtitle2" color="text.secondary" align="left">{formatDate(task?.due_date || new Date())}</Typography>
            </Stack>
          </Visible>

          <Stack direction="row" justifyContent="space-between" alignItems="center" width="100%">

            {/* PRIORIDAD */}
            <CustomMenu options={options.priorities} size="small">
              <Label sx={{ mt: 1 }}
                icon={<Flag sx={{ color: `${resources?.priorities?.[task.priority]?.color}.light` }} />}
                title={resources?.priorities?.[task?.priority]?.label}
                backgroundColor={`${resources?.priorities?.[task.priority]?.color}.main`}
              />
            </CustomMenu>

            {/* ESTADO */}
            <CustomMenu options={options.status} size="small">
              <Tooltip title="Cambiar estado" arrow placement="top">
                <IconButton>
                  <RadioButtonChecked sx={{ color: "text.secondary" }} />
                </IconButton>
              </Tooltip>
            </CustomMenu>

          </Stack>

        </Stack>
      </Stack>

      <ModalEdit isShowing={modals.editTask.isOpen}
        hide={modals.editTask.close}
        header="Editar tarea"
        task={task}
        onSubmit={(data) => updateTask(task?.id, data)} />
    </>
  )
}

export default TasksCard