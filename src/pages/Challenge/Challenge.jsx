﻿import React from 'react'

// UI COMPONENTS
import { Stack, Typography } from '@mui/material'
import KanbanBoard from './components/Kanban'
import Navbar from '../../components/common/Navbar'

// ICONS

// APIS

// PROVIDERS

// UTILS


function Challenge() {
  return (
    <>
      <Navbar />
      <Stack mt={10} justifyContent="start" alignItems="center">
        <KanbanBoard />
      </Stack>
    </>
  )
}

export default Challenge