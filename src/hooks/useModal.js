import { useState } from 'react'

/**
 * Crea un hook personalizado para gestionar el estado de un modal.
 *
 * @return {Object} Un objeto con propiedades y métodos para controlar el estado del modal.
*/
const useModal = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [args, setArgs] = useState(null);

  const open = (arg = null) => {
    setArgs(arg);
    setIsOpen(true);
  };

  const close = () => {
    setIsOpen(false);
    setArgs(null); // Reiniciar el índice seleccionado al cerrar
  };

  return {
    isOpen,
    args, // Retornar el índice seleccionado
    open,
    close,
  };
};

export default useModal;