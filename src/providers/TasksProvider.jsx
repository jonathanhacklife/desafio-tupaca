﻿import React, { createContext, useState, useContext } from 'react'

const TasksContext = createContext({})

export const useTasks = () => useContext(TasksContext)

export const TasksProvider = ({ children }) => {
  const [tasks, setTasks] = useState([])

  const value = {
    tasks,
    setTasks,
  }

  return (
    <TasksContext.Provider value={value}>
      {children}
    </TasksContext.Provider>
  )
}

export default TasksContext