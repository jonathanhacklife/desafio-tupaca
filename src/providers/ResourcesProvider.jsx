﻿import React, { createContext, useState, useContext } from 'react'

const ResourcesContext = createContext({})

export const useResources = () => useContext(ResourcesContext)

export const ResourcesProvider = ({ children }) => {
  const [resources, setResources] = useState([])

  const value = {
    resources,
    setResources,
  }

  return (
    <ResourcesContext.Provider value={value}>
      {children}
    </ResourcesContext.Provider>
  )
}

export default ResourcesContext