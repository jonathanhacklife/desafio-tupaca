﻿import React from 'react'

// UI COMPONENTS
import ScrollTop from '../components/common/ScrollTop'
import Challenge from '../pages/Challenge/Challenge'
import Home from '../pages/Home/Home'
import Unauthorized from '../pages/Unauthorized'
import theme from '../styles/style'

// ICONS

// APIS

// PROVIDERS
import { CssBaseline, ThemeProvider } from '@mui/material'
import { ResourcesProvider } from '../providers/ResourcesProvider'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'

// UTILS
import { BrowserRouter, Route, Routes } from 'react-router-dom'


export default function RouterManager() {
  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <ResourcesProvider>
          <CssBaseline />
          <BrowserRouter>
            <ScrollTop />
            <Routes>
              {/* PUBLIC ROUTES */}
              <Route index path="/" element={<Home />} />
              <Route path='*' element={<Unauthorized />} />

              {/* ADMIN */}
              <Route path="/challenge" element={<Challenge />} />
            </Routes>
          </BrowserRouter>
        </ResourcesProvider>
      </ThemeProvider>
    </QueryClientProvider>
  )
}
